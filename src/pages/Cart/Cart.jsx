import { useSelector } from "react-redux";
import Modal from "../../components/Modal/Modal";
import Product from "./CartItems";

function Cart() {
    const cart = useSelector((state) => state.cart);

    const styles = {
        fontSize: "28px",
        fontWeight: "600",
        marginBottom: "20px"
    }

    return (
        <>
        <h2 style={styles}>Ваша корзина:</h2>
        <div className="products">
            {cart.map(product => <Product key = {product.id} info={product} />)}
        </div>
              <Modal text = 'Ви впевнені, що хочете видалити цей товар з корзини?'/>
        </>
     );
}

export default Cart;